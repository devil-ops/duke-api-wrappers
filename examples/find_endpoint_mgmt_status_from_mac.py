#!/usr/bin/env python3

import sys
from pyplanisphere.helpers import get_credentials \
  as get_planisphere_credentials
from pyplanisphere.planisphere import api as planisphere_api

from devilparser import rcfile
from dukeservicenowpy.servicenow import api as servicenow_api

from dukeapiwrappers.helpers import find_sngroup_by

import dateparser


def main():

    if len(sys.argv) != 2:
        sys.stderr.write("Usage: %s MACADDRESS\n")
        return 2
    else:
        mac_address = sys.argv[1]

    credentials = get_planisphere_credentials()
    planisphere = planisphere_api(server='planisphere.oit.duke.edu',
                                  username=credentials['username'],
                                  key=credentials['key'])

    # Setup ServiceNow
    sn_instance = 'duke'
    configinfo = rcfile.parse('~/.servicenow.yaml', sn_instance).contents()
    sn_cred = {}
    sn_cred['username'] = configinfo['username']
    sn_cred['password'] = configinfo['password']
    servicenow = servicenow_api('%s.service-now.com' % 'duke', sn_cred)

    # Planisphere stuff
    active_since = int(dateparser.parse('30 days ago').timestamp())
    params = {
        'support_group_details': True,
        'mac_address': mac_address.replace('-', ':'),
        'show_endpoint_mgmt_info': True,
        'active_since': active_since
    }
    p_device = planisphere.get_devices(params=params)[0]
    p_support_group = p_device.get_support_group()
    if not p_support_group:
        sys.stderr.write("%s has no support group\n" % mac_address)
        return 0
    if p_device.get_endpoint_management_status():
        sys.stderr.write("%s is compliant now\n" % mac_address)
        return 0

    try:
        sn = find_sngroup_by(
            mac_address=mac_address,
            servicenow=servicenow,
            planisphere=planisphere
        )
    except Exception as e:
        sys.stderr.write("Could not look this one up in SN\n")
        sys.stderr.write(" %s\n" % mac_address)
        sys.stderr.write("%s\n" % e)
        return 1

    if not sn:
        sys.stderr.write("Could not match a SN group for %s\n" % mac_address)
        return 2

    print("%s|%s|%s" % (mac_address, sn.raw_data['name'],
                          p_support_group.data['key']))
    return 0


if __name__ == "__main__":
    sys.exit(main())
