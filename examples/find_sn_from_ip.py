#!/usr/bin/env python3

import sys
from pyplanisphere.helpers import get_credentials \
  as get_planisphere_credentials
from pyplanisphere.planisphere import api as planisphere_api

from devilparser import rcfile
from dukeservicenowpy.servicenow import api as servicenow_api

from cartographer_api.helpers import get_credentials \
    as get_cartographer_credentials
from cartographer_api.cartographer import Cartographer

from dukeapiwrappers.helpers import find_sngroup_by


def main():

    if len(sys.argv) != 2:
        sys.stderr.write("Usage: %s IPADDRESS\n")
        return 2
    else:
        ipaddress = sys.argv[1]

    credentials = get_planisphere_credentials()
    planisphere = planisphere_api(server='planisphere.oit.duke.edu',
                                  username=credentials['username'],
                                  key=credentials['key'])

    creds = get_cartographer_credentials()
    cartographer = Cartographer(creds['user'], creds['key'],
                                creds['host'])

    # Setup ServiceNow
    sn_instance = 'duke'
    configinfo = rcfile.parse('~/.servicenow.yaml', sn_instance).contents()
    sn_cred = {}
    sn_cred['username'] = configinfo['username']
    sn_cred['password'] = configinfo['password']
    servicenow = servicenow_api('%s.service-now.com' % 'duke', sn_cred)

    sn_group = find_sngroup_by(
        ipaddress=ipaddress,
        cartographer=cartographer,
        servicenow=servicenow,
        planisphere=planisphere
    )
    print(sn_group)

    return 0


if __name__ == "__main__":
    sys.exit(main())
