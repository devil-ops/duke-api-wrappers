#!/usr/bin/env python3

import sys
import argparse
import dateparser
import yaml
from pyplanisphere.helpers import get_credentials \
  as get_planisphere_credentials
from pyplanisphere.planisphere import api as planisphere_api

from devilparser import rcfile
from dukeservicenowpy.servicenow import api as servicenow_api

from cartographer_api.helpers import get_credentials \
    as get_cartographer_credentials
from cartographer_api.cartographer import Cartographer


def most_common(lst):
    return max(set(lst), key=lst.count)


def parse_args():
    parser = argparse.ArgumentParser(
        description='Find and notify an owner of something')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-m', '--mac-address', type=str,
                       help="Search on MAC Address")
    group.add_argument('-i', '--ipaddress', type=str,
                       help="Search on IP Address")
    parser.add_argument('-w', '--when', type=str, default='Yesterday',
                        help="When to query (For IPs)"
                        )
    parser.add_argument('-o', '--override-file', type=str,
                        help='Override file for groups'
                        )

    return parser.parse_args()


def main():
    args = parse_args()

    # Load up override info if needed
    if args.override_file:
        with open(args.override_file, 'r') as stream:
            override_info = yaml.load(stream)
    else:
        override_info = {
            'support_groups': {}
        }

    credentials = get_planisphere_credentials()
    planisphere = planisphere_api(server='planisphere.oit.duke.edu',
                                  username=credentials['username'],
                                  key=credentials['key'])

    # Setup ServiceNow
    sn_instance = 'duke'
    configinfo = rcfile.parse('~/.servicenow.yaml', sn_instance).contents()
    sn_cred = {}
    sn_cred['username'] = configinfo['username']
    sn_cred['password'] = configinfo['password']
    servicenow = servicenow_api('%s.service-now.com' % 'duke', sn_cred)

    # Make sure we have a MAC Address
    if args.mac_address:
        mac_address = args.mac_address
    elif args.ipaddress:
        creds = get_cartographer_credentials()
        cartographer = Cartographer(creds['user'], creds['key'],
                                    creds['host'])
        mac_address = cartographer.ip_to_mac_at(
            ipaddress=args.ipaddress,
            when=dateparser.parse(args.when))

    params = {
        'support_group_details': True,
        'show_mac_addresses': True,
        'user_details': True,
        'mac_address': mac_address.replace('-', ':')
    }
    devices = planisphere.get_devices(params=params)
    if len(devices) > 1:
        sys.stderr.write("Uh oh, i dunno how to do multiple devices yet\n")
        sys.exit(2)
    elif len(devices) == 0:
        sys.stderr.write("No devices found :(\n")
        sys.exit(2)

    device = devices[0]

    support_group = device.get_support_group()
    if support_group:

        sn_groups = []
        for netid in support_group.user_netids:
            sn_user = servicenow.get_user_from_name(netid)
            sn_groups.extend(sn_user.get_groups(attribute='name',
                                                group_max_users=20))

        if support_group.key in override_info['support_groups']:
            print(override_info['support_groups'][support_group.key])
        else:
            print(most_common(sn_groups))

    else:
        print((
            "Couldn't find a support group, but here is some more info on the"
            " device:"
        ))
        for k, v in device.__dict__['data'].items():
            print(k, v)

    return 0


if __name__ == "__main__":
    sys.exit(main())
