#!/usr/bin/env python3

import sys
import pprint
import argparse
from datetime import datetime
import dateparser
from pyplanisphere.helpers import get_credentials \
  as get_planisphere_credentials
from pyplanisphere.planisphere import api as planisphere_api

from devilparser import rcfile
from dukeservicenowpy.servicenow import api as servicenow_api

from cartographer_api.helpers import get_credentials \
    as get_cartographer_credentials
from cartographer_api.cartographer import Cartographer
import jinja2
import os


def most_common(lst):
    return max(set(lst), key=lst.count)


def render(tpl_path, context):
    path, filename = os.path.split(tpl_path)
    return jinja2.Environment(
        loader=jinja2.FileSystemLoader(path or './')
    ).get_template(filename).render(context)


def parse_args():
    parser = argparse.ArgumentParser(
        description='Find and notify an owner of something')
    parser.add_argument('target', type=str,
                        help="Target to search for.  Can be IP or MAC")
    parser.add_argument('-w', '--when', type=str, default='Yesterday',
                        help="When to query (For IPs)"
                        )
    parser.add_argument(
        '-d', '--default-sn-group', type=str, default='Security-University',
        help="Default ServiceNow Group, if it can't be detected"
    )
    parser.add_argument(
        '-i', '--it-service', type=str, default='Patch Management',
        help='IT Service for ticket'
    )
    parser.add_argument(
        '-t', '--template', type=argparse.FileType('r'), required=True,
        help='Template for Ticket (first line treated as short desc)'
    )
    parser.add_argument(
        '-c', '--check-only', action='store_true',
        help='Only check what would be sent, plus debug vars'
    )
    parser.add_argument(
        '-s', '--servicenow-instance', type=str, default='duke',
        choices=['duke', 'dukesandbox'],
        help='Duke ServiceNow instance to use'
    )
    parser.add_argument(
        '-u', '--due', type=str, default='in 2 days',
        help='Meta variable for {{ due }}. This should be a dateparser date'
    )
    return parser.parse_args()


def main():
    args = parse_args()

    credentials = get_planisphere_credentials()
    planisphere = planisphere_api(server='planisphere.oit.duke.edu',
                                  username=credentials['username'],
                                  key=credentials['key'])

    # Setup ServiceNow
    sn_instance = args.servicenow_instance
    configinfo = rcfile.parse('~/.servicenow.yaml', sn_instance).contents()
    sn_cred = {}
    sn_cred['username'] = configinfo['username']
    sn_cred['password'] = configinfo['password']
    servicenow = servicenow_api('%s.service-now.com' % sn_instance, sn_cred)

    # Make sure the default sn group is valid
    default_sn_group = servicenow.get_group(
        by='name', value=args.default_sn_group)

    # Check IT Service
    org = servicenow.get_organization(by='u_name', value='Duke University')
    it_service = servicenow.get_it_service(by='name', value=args.it_service,
                                           organization=org)
    # Cartographer stuff
    creds = get_cartographer_credentials()
    cartographer = Cartographer(creds['user'], creds['key'], creds['host'])

    # Make sure we have a MAC Address
    if (':' in args.target) or ('-' in args.target):
        mac_address = args.target.replace('-', ':')
        try:
            ipaddress = cartographer.mac_to_ip_at(
                mac=mac_address,
                when=dateparser.parse(args.when)
            )
        except Exception as e:
            sys.stderr.write("Could not look up IP because %s\n" % e)
            ipaddress = None
    else:
        ipaddress = args.target
        mac_address = cartographer.ip_to_mac_at(
            ipaddress=ipaddress,
            when=dateparser.parse(args.when))

    params = {
        'support_group_details': True,
        'show_mac_addresses': True,
        'show_oses': True,
        'show_subnets': True,
        'show_endpoint_mgmt_info': True,
        'user_details': True,
        'mac_address': mac_address.replace('-', ':'),
    }
    devices = planisphere.get_devices(params=params)
    if len(devices) > 1:
        sys.stderr.write("Uh oh, i dunno how to do multiple devices yet\n")
        sys.exit(2)

    device = devices[0]

    # Try and guess the support group
    support_group = device.get_support_group()
    likely_sn_group = None

    if support_group:
        sn_groups = []
        for netid in support_group.user_netids:
            sn_user = servicenow.get_user_from_name(netid)
            sn_groups.extend(sn_user.get_groups(attribute='name',
                                                group_max_users=20))

        likely_sn_group = most_common(sn_groups)
    else:
        likely_sn_group = None
        sys.stderr.write("No support group for this device, going to use the default\n")

    now = datetime.now()
    context = {
        'target': args.target,
        'ipaddress': ipaddress,
        'mac_address': mac_address,
        'planisphere_device': device.raw_data,
        'now': now,
        'due': dateparser.parse(args.due)
    }

    if args.check_only:
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(context)

    ticket_info_header, ticket_info_body = render(
        args.template.name, context).split('\n', 1)

    if likely_sn_group:
        sn_group = servicenow.get_group(by='name', value=likely_sn_group)
    else:
        sn_group = default_sn_group

    print("SN Group: {}".format(sn_group.name))
    print("Header: {}".format(ticket_info_header))
    print("Body: {}".format(ticket_info_body))

    if not args.check_only:
        ticket = servicenow.create_incident_ticket(
            short_description=ticket_info_header,
            description=ticket_info_body,
            organization='Duke University',
            caller_name='Duke IT Security',
            assignment_group=sn_group.name,
            it_service=it_service.name
        )

        print("Created {}".format(ticket))

    return 0


if __name__ == "__main__":
    sys.exit(main())
