# duke-api-wrappers

Library and scripts for interacting with the various Duke APIs

## overrides.yml

This file is used to override data that would normally be pulled from an API.
For instance, if you wanted to override the support group
'oit\_ssi\_systems\_linux' with the 'Blah' ServiceNow group, you would use:

```yaml
---
support_groups:
  duke:group-manager:roles:oit_ssi_systems_linux: 'Blah'
```

## ./scripts/ticket-in-ator.py

Used to generate tickets to owners, based on IP/Mac and a template.  Usage:

```
usage: ticket-in-ator.py [-h] [-w WHEN] -d DEFAULT_SN_GROUP [-i IT_SERVICE] -t
                         TEMPLATE [-s {duke,dukesandbox}]
                         target

Find and notify an owner of something

positional arguments:
  target                Target to search for. Can be IP or MAC

optional arguments:
  -h, --help            show this help message and exit
  -w WHEN, --when WHEN  When to query (For IPs)
  -d DEFAULT_SN_GROUP, --default-sn-group DEFAULT_SN_GROUP
                        Default ServiceNow Group, if it can't be detected
  -i IT_SERVICE, --it-service IT_SERVICE
                        IT Service for ticket
  -t TEMPLATE, --template TEMPLATE
                        Template for Ticket (first line treated as short desc)
  -s {duke,dukesandbox}, --servicenow-instance {duke,dukesandbox}
                        Duke ServiceNow instance to use
```
