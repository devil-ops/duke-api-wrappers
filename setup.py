import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


with open('requirements.txt') as requirements_file:
    install_requirements = requirements_file.read().splitlines()

setup(
    name="duke-api-wrappers",
    version="0.0.22",
    author="Drew Stinnett",
    author_email="drew.stinnett@duke.edu",
    description=("Package for tying Duke automation apis together"),
    keywords="planisphere servicenow duke cartographer",
    packages=find_packages(),
    install_requires=install_requirements,
    scripts=[
        'scripts/ticket-in-ator.py'
    ],
    long_description=read('README.md'),
)
