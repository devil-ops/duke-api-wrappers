import dateparser
import yaml
import logging


def most_common(lst):
    return max(set(lst), key=lst.count)


def find_sngroup_by(*args, **kwargs):
    """
    Given a MAC or IPADDRESS, return a servicenow group

    Args:
      mac_address           str: MAC Address to look up
      ipaddress             str: IP Address to look up
      override_file         str: File containing overrides
      planisphere   Planisphere: API Object for Planisphere
      cartographer Cartographer: Cartographer API object
      servicenow     ServiceNow: ServiceNow API object

    Returns:
      ServiceNowGroup: ServiceNow group

    """
    mac_address = kwargs.get('mac_address', None)
    ipaddress = kwargs.get('ipaddress', None)
    cartographer = kwargs.get('cartographer', None)
    planisphere = kwargs.get('planisphere')
    servicenow = kwargs.get('servicenow')
    override_file = kwargs.get('override_file', None)
    when = kwargs.get('when', 'Yesterday')

    if override_file:
        with open(override_file, 'r') as stream:
            override_info = yaml.load(stream)
    else:
        override_info = {
            'support_groups': {}
        }

    # Make sure we have a MAC Address
    if ipaddress:
        mac_address = cartographer.ip_to_mac_at(
            ipaddress=ipaddress,
            when=dateparser.parse(when))

    active_since = int(dateparser.parse('30 days ago').timestamp())
    params = {
        'support_group_details': True,
        'show_mac_addresses': True,
        'user_details': True,
        'mac_address': mac_address.replace('-', ':'),
        'active_since': active_since
    }
    devices = planisphere.get_devices(params=params)

    if len(devices) > 1:
        most_recent = float(0)
        most_recent_device = None
        import time
        for device in devices:
            active_time = time.mktime(
                device.data['mac_addresses'][0]['last_active'].timetuple())
            if active_time > most_recent:
                most_recent = active_time
                most_recent_device = device
        device = most_recent_device
    else:
        device = devices[0]

    support_group = device.get_support_group()
    if not support_group:
        return None

    sn_groups = []
    for netid in support_group.user_netids:
        try:
            sn_user = servicenow.get_user_from_name(netid)
        except Exception as e:
            logging.debug("Could not find SN user %s : %s" % (netid, e))
            continue
        sn_groups.extend(sn_user.get_groups(attribute='name'))

    if support_group.key in override_info['support_groups']:
        sn_group_name = override_info['support_groups'][support_group.key]
    else:
        sn_group_name = most_common(sn_groups)

    return(servicenow.get_group(by='name', value=sn_group_name))
